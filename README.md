# week2

## Lesson November 14th, 2022

1. Available projects: [pdf](./slides/Projects_available.pdf)
2. numpy library: [notebook](./scripts/numpy_library.ipynb) and [pdf](./slides/library_numpy.pdf)

## Lesson November 16th, 2022

1. Introduction to Natural Language Processing 
2. seaborn library: [notebook](./scripts/seaborn_visualization_parta.ipynb) and [notebook](./scripts/seaborn_visualization_partb.ipynb)

Use cases:

3. pollutants data exploration: [notebook](./use_cases/air_quality_data_visualization.ipynb) and [pdf](./scripts/air_quality_data_visualization.pdf)
4. california houses price data exploration: [notebook](./use_cases/california_housing_price_data_exploration.ipynb) and [pdf](slides/california_housing_price_data_exploration.pdf)

**NOTE**: Seaborn library is used in the notebooks that are in the `use_cases` folder and in scripts, therefore the use of this library will be explained when necessary. The specific seaborn notebooks can be used to deepen its usage.  

## Lesson November 17th, 2022

1. Introduction to Natural Language Processing: [pdf](intro_nlp.pdf)
2. Works with strings: [notebook](./scripts/working_with_strings.ipynb) and [pdf](./slides/working_with_strings.pdf)
3. Regular Expression: [notebook](./scripts/regular_expression.ipynb) and [pdf](./slides/regular_expression.pdf)
4. Cleaning texts
5. Bag Of Words
6. Tokenization

Use cases:

7. Solution of exercises in [air quality data visualization notebook](./use_cases/air_quality_data_visualization.ipynb): [notebook](./solution/air_quality_data_visualization_exercise_solution.ipynb), [notebook](./solution/save_plots.ipynb)
8. processing zonato customers' text comments: [notebook](./use_cases/processing_zonato_customers_comments.ipynb) and [pdf](./use_cases/processing_zonato_customers_comments.pdf) 

